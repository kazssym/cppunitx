Exceptions
==========

.. cpp:namespace:: cppunitx

.. cpp:class:: AssertionError : public std::runtime_error

   This exception class indicates an assertion failure was detected.
